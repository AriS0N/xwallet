//
//  Languages.swift
//  XWallet
//
//  Created by loj on 31.07.17.
//

import Foundation


public enum Languages {
    
    case English
    case German
    case Spanish
    
}
