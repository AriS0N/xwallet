//
//  Constants.swift
//  XWallet
//
//  Created by loj on 22.10.17.
//

import Foundation

public class Constants {
    
    public static let defaultLanguage = "English"
    public static let defaultCurrency = "USD"
    
    public static let pinCodeLength = 6
    
    public static let defaultWalletName = "xwallet"
    public static let defaultWalletLanguage = "English"
    
    // Size in bytes for trx go be accepted
    public static let defaultUpperTransactionSizeLimit: UInt64 = 100_000
    
    public static let atomicUnitsPerMonero: UInt64 = 1_000_000_000_000
    public static let numberOfFractionDigits: Int = 12
    public static let prettyPrintNumberOfFractionDigits = 7
    
    public static let mixinCount: UInt32 = 7
    public static let defaultTransactionPriority = PendingTransactionPriority_Low
    
    public static let numberOfRequiredConfirmations: UInt64 = 10
    
    public static let defaultNodeAddress = "165.227.246.15:18089"
    public static let defaultNodeUserId = "imperfectelephant"
    public static let defaultNodePassword = "1oCejlUHR4wYTjoG9j2KBk24DXOBxV"
    
    public static let donationWalletAddress = "48u79gBhhdo6Pts6daXfvn7fQ2QL9BhaqNfqTgzbgGu5fJVaX7zjTVjNXaHtj71w3y81cc9vcuH7rNiz37BC9hQuUKEcoiU"
    
    public static let fiatProviderUri = "https://min-api.cryptocompare.com/data/pricemulti?fsyms=XMR&tsyms=%@"
    public static let fiatUpdateIntervalInSeconds = 300
    
    public static let feeProviderUri = "http://node.moneroworld.com:18089/json_rpc"
    public static let feeUpdateIntervalInSeconds = 300
    public static let estimatedMessageSizeInKB: Int = 13
}

