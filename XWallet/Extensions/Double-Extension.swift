//
//  Double-Extension.swift
//  XWallet
//
//  Created by loj on 23.03.18.
//

import Foundation


extension Double {
    
    private static let exactNumberOfFractionDigits = 2
    
    public func toCurrency() -> String {
        guard let currency = Double.currencyFormatter.string(from: NSNumber(value: self)) else {
            return ""
        }
        return currency
    }
    
    private static let currencyFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.allowsFloats = true
        numberFormatter.minimumIntegerDigits = 1
        numberFormatter.minimumFractionDigits = exactNumberOfFractionDigits
        numberFormatter.maximumFractionDigits = exactNumberOfFractionDigits
        return numberFormatter
    }()
}
