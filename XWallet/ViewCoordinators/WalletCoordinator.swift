//
//  WalletCoordinator.swift
//  XWallet
//
//  Created by loj on 16.11.17.
//

import Foundation
import UIKit


public protocol WalletCoordinatorDelegate: class {
    func walletCoordinatorWalletNuked(walletCoordinator: WalletCoordinator)
}


public class WalletCoordinator: Coordinator {
    
    private let storyboardName = "Wallet"
    private let walletSceneName = "Wallet"
    private let ReceiveSceneName = "Receive"
    private let RecipientSceneName = "Recipient"
    private let AmountSceneName = "Amount"
    private let PaymentIdSceneName = "PaymentId"
    private let SummarySceneName = "Summary"
    private let ScanSceneName = "Scan"
    
    private let pinStoryBoardName = "Onboarding"
    private let pinSceneName = "PIN"

    // This is the `root` view controller of this coordinator
    private var walletVC: WalletVC!
    
    private var moneroBag: MoneroBagProtocol
    private var walletLifecycleService: WalletLifecycleServiceProtocol
    private var propertyStore: PropertyStoreProtocol
    private var secureStore: SecureStoreProtocol
    private var fiatService: FiatServiceProtocol
    private var feeService: FeeServiceProtocol!

    private let navigationController: UINavigationController
    public var childCoordinators: [Coordinator] = []

    weak var delegate: WalletCoordinatorDelegate?
    
    private lazy var storyboard: UIStoryboard = {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard
    }()
    
    private lazy var storyboardPin: UIStoryboard = {
        let storyboard = UIStoryboard(name: pinStoryBoardName, bundle: nil)
        return storyboard
    }()
    
    init(navigationController: UINavigationController,
         moneroBag: MoneroBagProtocol,
         walletLifecycleService: WalletLifecycleServiceProtocol,
         propertyStore: PropertyStoreProtocol,
         secureStore: SecureStoreProtocol,
         fiatService: FiatServiceProtocol,
         feeService: FeeServiceProtocol)
    {
        self.navigationController = navigationController
        self.moneroBag = moneroBag
        self.walletLifecycleService = walletLifecycleService
        self.propertyStore = propertyStore
        self.secureStore = secureStore
        self.fiatService = fiatService
        self.feeService = feeService
    }
    
    func start() {
        self.showWalletViewController()
        self.startFiatUpdates()
        self.startFeeEstimationUpdates()
    }
    
    private func showWalletViewController() {
        self.walletVC = self.storyboard.instantiateViewController(withIdentifier: walletSceneName) as! WalletVC
        walletVC.delegate = self
        walletVC.viewTitle = "X WALLET"
        walletVC.viewTitleSyncing = "SYNCING"
        walletVC.configButtonTitle = ""
        walletVC.sendButtonTitle = "Send"
        walletVC.receiveButtonTitle = "Receive"
        walletVC.viewModel = self.getWalletViewModel()
        self.navigationController.pushViewController(walletVC, animated: true)
    }
    
    private func getWalletViewModel() -> WalletViewModel? {
        guard let wallet = self.moneroBag.wallet  else {
            return nil
        }
        
        let xmrBalance = XMRFormatter.format(atomicAmount: wallet.balance,
                                             numberOfFractionDigits: Constants.prettyPrintNumberOfFractionDigits)
        let history = wallet.history
        let hasLockedBalance = wallet.balance != wallet.unlockedBalance
        
        let walletViewModel = WalletViewModel(xmrAmount: xmrBalance,
                                              otherAmount: self.otherAmount(forXMRValue: self.moneroBag.wallet?.balance),
                                              otherCurrency: self.propertyStore.currency,
                                              history: history.all,
                                              hasLockedBalance: hasLockedBalance)
        return walletViewModel
    }
    
    private func otherAmount(forXMRValue xmrValue: UInt64?) -> String {
        guard let xmrValue = xmrValue else {
            return "---"
        }
        
        let fiatEquivalent = self.fiatService.getFiat(forXMRValue: xmrValue)
        switch fiatEquivalent {
        case .none:
            return "---"
        case let .value(.recent, amount):
            return "\(amount.toCurrency())"
        case let .value(age, amount):
            return "\(amount.toCurrency()) [\(age.toString())]"
        }
    }
    
    private func startFiatUpdates() {
        self.fiatService.startUpdating(withIntervalInSeconds: Constants.fiatUpdateIntervalInSeconds,
                                       notificationHandler: self.fiatValueUpdated)
    }
    
    private func stopFiatUpdates() {
        self.fiatService.stopUpdating()
    }
    
    private func fiatValueUpdated() {
        let vm = self.getWalletViewModel()
        DispatchQueue.main.async {
            self.walletVC.viewModel = vm
            self.walletVC.viewModelIsUpdated()
        }
    }
    
    private func startFeeEstimationUpdates() {
        self.feeService.startUpdating(withIntervalInSeconds: Constants.feeUpdateIntervalInSeconds,
                                                notificationHandler: {} )
    }
    
    private func stopFeeEstimationUpdates() {
        self.feeService.stopUpdating()
    }
    
    private func showReceiveViewController() {
        let vc = self.storyboard.instantiateViewController(withIdentifier: ReceiveSceneName) as! ReceiveVC
        vc.delegate = self
        vc.qrcImage = QRCGenerator.generate(from: self.moneroBag.wallet?.publicAddress?.address)
        vc.walletAddress = self.moneroBag.wallet?.publicAddress?.address
        vc.backButtonTitle = "<"
        vc.viewTitle = "RECEIVE"
        vc.copyButtonTitle = "Copy Address to Clipboard"
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    private func showRecipientViewController() {
        let vc = self.storyboard.instantiateViewController(withIdentifier: RecipientSceneName) as! ReceipientVC
        vc.delegate = self
        vc.backButtonTitle = "<"
        vc.viewTitle = "SEND"
        vc.subTitle = "Recipient Address"
        vc.instructionText = "Please choose a method"
        vc.scanQRCodeButtonTitle = "Scan QR Code"
        vc.pasteFromClipboardButtonTitle = "Paste from Clipboard"
        vc.sendToDeveloperButtonTitle = "Send to Developer"
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    private func showSettings() {
        let settingsCoordinator = SettingsCoordinator(navigationController: self.navigationController,
                                                      propertyStore: self.propertyStore,
                                                      secureStore: self.secureStore,
                                                      moneroBag: self.moneroBag,
                                                      walletLifecycleService: self.walletLifecycleService)
        settingsCoordinator.delegate = self
        settingsCoordinator.start()
        
        self.add(childCoordinator: settingsCoordinator)
    }
}


extension WalletCoordinator: WalletDelegate {
    
    public func walletUpdated() {
        let vm = self.getWalletViewModel()
        
        DispatchQueue.main.async {
            self.walletVC.viewModel = vm
            self.walletVC.viewModelIsUpdated()
        }
    }

    public func walletSyncing(initialHeight: UInt64, walletHeight: UInt64, blockChainHeight: UInt64) {
        DispatchQueue.main.async {
            self.walletVC.walletSyncing(initialHeight: initialHeight,
                                        walletHeight: walletHeight,
                                        blockChainHeight: blockChainHeight)
        }
    }
    
    public func walletSyncCompleted() {
        DispatchQueue.main.async {
            self.walletVC.walletSyncCompleted()
        }
    }
}


extension WalletCoordinator: WalletVCDelegate {
    
    func walletVCSettingsButtonTouched() {
        self.stopFiatUpdates()
        self.showSettings()
    }
    
    func walletVCSendTouched() {
        self.moneroBag.payment = nil
        self.showRecipientViewController()
    }
    
    func walletVCReceiveTouched() {
        self.showReceiveViewController()
    }

    func walletVCWillEnterForeground() {
        if let wallet = self.moneroBag.wallet {
            wallet.register(delegate: self)
        }
    }
}


extension WalletCoordinator: ReceiveVCDelegate {

    func receiveVCBackTouched() {
        self.navigationController.popViewController(animated: true)
    }
    
    func receiveVCCopyToClipboardTouched() {
        if let walletAddress = self.moneroBag.wallet?.publicAddress?.address {
            UIPasteboard.general.string = walletAddress
        }
    }
}


extension WalletCoordinator: ReceipientVCDelegate {
    
    func receipientVCBackTouched() {
        self.navigationController.popViewController(animated: true)
    }
    
    func receipientVCScanQRCodeTouched() {
        self.moneroBag.payment = self.getEmptyPayment()
        
        let vc = self.storyboard.instantiateViewController(withIdentifier: ScanSceneName) as! ScanVC
        vc.delegate = self
        vc.backButtonTitle = "<"
        vc.viewTitle = "SCAN"
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    func receipientVCPasteFromClipboardTouched(viewController: ReceipientVC) {
        guard let targetAddress = UIPasteboard.general.string else {
            viewController.show(message: "Pasteboard is empty. Please open other app and copy the target wallet address into the pasteboard and then come back here.")
            return
        }

        if !self.isValid(walletAddress: targetAddress) {
            viewController.show(message: "The pasteboard does not contain a valid Monero wallet address.")
            return
        }
        
        self.moneroBag.payment = self.getEmptyPayment()
        self.moneroBag.payment?.targetAddress = targetAddress
        self.showAmountViewController()
    }
    
    func receipientVCSendToDeveloperTouched(viewController: ReceipientVC) {
        self.moneroBag.payment = self.getEmptyPayment()
        self.moneroBag.payment?.targetAddress = Constants.donationWalletAddress
        self.showAmountViewController()
    }
    
    private func showAmountViewController() {
        let vc = self.storyboard.instantiateViewController(withIdentifier: AmountSceneName) as! AmountVC
        vc.delegate = self
        vc.backButtonTitle = "<"
        vc.nextButtonTitle = "Next"
        vc.viewTitle = "SEND"
        vc.currencyText = "XMR"
        vc.amountText = "0"
        vc.amountOtherText = "\(self.propertyStore.currency) \(self.otherAmount(forXMRValue: 0))"
        
        let formattedAmounts = self.formattedAmounts()
        vc.totalAmountAvailableButtonTitle = "XMR \(formattedAmounts.available)"
        vc.balanceTitle = "Balance"
        vc.balanceValueText = formattedAmounts.balance
        vc.estimatedFeeTitle = "Estimated Fee"
        vc.estimatedFeeValueText = formattedAmounts.fee
        
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    private func getEmptyPayment() -> PaymentProtocol {
        return Payment()
    }
    
    private func amounts() -> (balance: UInt64, fee: UInt64, available: UInt64) {
        let balance = self.moneroBag.wallet?.balance ?? 0
        let fee = self.feeService.getFeeInAtomicUnits(forMessageSizeInKB: Constants.estimatedMessageSizeInKB)
        let available = balance > fee ? balance - fee : 0

        return (balance: balance, fee: fee, available: available)
    }
    
    private func formattedAmounts() -> (balance: String, fee: String, available: String) {
        let amounts = self.amounts()
        
        let balanceFormatted = XMRFormatter.format(atomicAmount: amounts.balance, numberOfFractionDigits: Constants.numberOfFractionDigits)
        let feeFormatted = XMRFormatter.format(atomicAmount: amounts.fee, numberOfFractionDigits: Constants.numberOfFractionDigits)
        let availableFormatted = XMRFormatter.format(atomicAmount: amounts.available, numberOfFractionDigits: Constants.numberOfFractionDigits)

        return (balance: balanceFormatted, fee: feeFormatted, available: availableFormatted)
    }
}


extension WalletCoordinator: ScanVCDelegate {
    
    public func scanVCDelegateBackButtonTouched() {
        self.navigationController.popViewController(animated: true)
    }
    
    public func scanVCDelegateAddressDetected(address: String, viewController: ScanVC) {
        print("scanned: \(address)")
        if self.isValid(walletAddress: address) {
            self.moneroBag.payment = self.getEmptyPayment()
            self.moneroBag.payment?.targetAddress = address
            self.navigationController.popViewController(animated: true)
            self.showAmountViewController()
        } else {
            viewController.show(message: "This is not a valid Monero wallet address.")
        }
    }
    
    public func scanVCDelegatePopupDismissed(viewController: ScanVC) {
        print("scanner dismissed")
        viewController.startScanning()
    }
    
    private func isValid(walletAddress: String) -> Bool {
        let isValidWalletAddress = monero_isValidWalletAddress(walletAddress)
        return isValidWalletAddress
    }
}


extension WalletCoordinator: AmountVCDelegate {
    
    func amountVCBackTouched() {
        self.navigationController.popViewController(animated: true)
    }
    
    func amountVCNextButtonTouched(formattedAmount: String, viewController: AmountVC) {
        self.moneroBag.payment?.amountInAtomicUnits = XMRFormatter.fromFormatted(amount: formattedAmount)
        self.showPaymentIdViewController()
    }
    
    func amountVCTotalAmountButtonTouched(viewController: AmountVC) {
        let amounts = self.amounts()
        let formattedAmounts = self.formattedAmounts()
        
        viewController.amountText = formattedAmounts.available
        viewController.amountOtherText = "\(self.propertyStore.currency) \(self.otherAmount(forXMRValue: amounts.available))"
        viewController.refresh()
        viewController.nextAllowed()
    }
    
    func amountVCAmountValueChanged(amount: Double?, viewController: AmountVC) {
        guard let requestedXmrDouble = amount else {
            viewController.showFiatValue("---", forCurrency: self.propertyStore.currency)
            viewController.nextNotAllowed()
            return
        }
        
        let requestedXmrInAtomicUnits = UInt64(requestedXmrDouble * Double(Constants.atomicUnitsPerMonero))
        let fiatValue = self.otherAmount(forXMRValue: requestedXmrInAtomicUnits)
        viewController.showFiatValue(fiatValue, forCurrency: self.propertyStore.currency)
        
        let available = self.moneroBag.wallet?.balance ?? 0
        if requestedXmrInAtomicUnits > 0 && requestedXmrInAtomicUnits <= available {
            viewController.nextAllowed()
        } else {
            viewController.nextNotAllowed()
        }
    }
    
    private func showPaymentIdViewController() {
        let vc = self.storyboard.instantiateViewController(withIdentifier: PaymentIdSceneName) as! PaymentIdVC
        vc.delegate = self
        vc.backButtonTitle = "<"
        vc.nextButtonTitle = "Next"
        vc.viewTitle = "SEND"
        vc.subTitle = "Payment ID"
        vc.instructionText = "Please enter a payment ID if needed."
        vc.paymentIdPlaceholderText = "Payment ID"
        vc.paymentIdText = ""
        vc.pasteFromClipboardButtonTitle = "Paste"
        self.navigationController.pushViewController(vc, animated: true)
    }
}


extension WalletCoordinator: PaymentIdVCDelegate {
    
    func paymentIdVCBackButtonTouched() {
        self.navigationController.popViewController(animated: true)
    }
    
    func paymentIdVCNextButtonTouched(paymentId: String, viewController: PaymentIdVC) {
        if !self.isValid(paymentId: paymentId) {
            viewController.show(message: "Your payment ID is invalid. Please change it or leave it empty.")
            return
        }
        self.moneroBag.payment?.paymentId = paymentId

        self.prepareSummaryViewController(viewController: viewController)
    }
    
    private func prepareSummaryViewController(viewController: ActivityIndicatorProtocol) {
        guard let amount = self.moneroBag.payment?.amountInAtomicUnits else {
            return
        }
        
        viewController.showActivityIndicator()
        
        DispatchQueue.global(qos: .background).async {
            let key = monero_createTransaction(self.moneroBag.payment?.targetAddress,
                                               self.moneroBag.payment?.paymentId,
                                               amount,
                                               Constants.mixinCount,
                                               Constants.defaultTransactionPriority)
            self.moneroBag.payment?.keyOfPendingTransaction = key
            let networkFee = monero_getTransactionFee(key)
            self.moneroBag.payment?.networkFeeInAtomicUnits = networkFee < 0 ? UInt64(0) : UInt64(networkFee)

            DispatchQueue.main.async {
                viewController.hideActivityIndicator()
                self.showSummaryViewController()
            }
        }
    }
    
    func paymentIdVCPasteFromClipboardButtonTouched(viewController: PaymentIdVC) {
        guard let clipboardString = UIPasteboard.general.string else { return }
        
        if self.isValid(paymentId: clipboardString) {
            viewController.paymentIdText = clipboardString
            viewController.refresh()
        } else {
            viewController.show(message: "Not a valid payment ID in pasteboard")
        }
    }
    
    private func isValid(paymentId: String) -> Bool {
        if paymentId == "" {
            return true
        }
        
        let isValid = monero_isValidPaymentId(paymentId)
        return isValid
    }
    
    private func showSummaryViewController() {
        let vc = self.storyboard.instantiateViewController(withIdentifier: SummarySceneName) as! SummaryVC
        vc.delegate = self
        vc.backButtonTitle = "<"
        vc.viewTitle = "SEND"
        vc.subTitle = "Summary"
        vc.addressText = "Address"
        vc.addressValueText = self.moneroBag.payment?.targetAddress ?? "---"
        vc.paymentIdText = "Payment ID"
        vc.paymentIdValueText = self.moneroBag.payment?.paymentId ?? "---"
        vc.subtotalText = "Subtotal"
        let amount = XMRFormatter.format(atomicAmount: self.moneroBag.payment?.amountInAtomicUnits ?? 0,
                                         numberOfFractionDigits: Constants.numberOfFractionDigits)
        vc.subtotalValueText = "XMR \(amount)"
        vc.networkFeeText = "Network Fee"
        let networkFee = XMRFormatter.format(atomicAmount: self.moneroBag.payment?.networkFeeInAtomicUnits ?? 0,
                                             numberOfFractionDigits: Constants.numberOfFractionDigits)
        vc.networkFeeValueText = "XMR \(networkFee)"
        vc.confirmButtonTitle = "Confirm"
        vc.totalText = "Total"
        let totalAmount = XMRFormatter.format(atomicAmount: self.moneroBag.payment?.amountTotalInAtomicUnits ?? 0,
                                              numberOfFractionDigits: Constants.numberOfFractionDigits)
        vc.totalValueText = "XMR \(totalAmount)"
        vc.balanceIsSufficient = self.balanceIsSufficient()
        vc.balanaceInsufficientText = "Your balance is too low"
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    private func balanceIsSufficient() -> Bool {
        guard let key = self.moneroBag.payment?.keyOfPendingTransaction else {
            return false
        }
        if key < 0 {
            return false
        }
        
        let available = self.moneroBag.wallet?.balance ?? 0
        let requested = self.moneroBag.payment?.amountTotalInAtomicUnits ?? 0
        
        return available >= requested
    }
}


extension WalletCoordinator: SummaryVCDelegate {
    
    func summaryVCConfirmButtonTouched() {
        self.showConfirmWithPinViewController()
    }
    
    func summaryVCBackButtonTouched() {
        self.navigationController.popViewController(animated: true)
    }
    
    private func showConfirmWithPinViewController() {
        guard let appPin = self.secureStore.appPin else {
            return
        }
        
        let vc = self.storyboardPin.instantiateViewController(withIdentifier: pinSceneName) as! PinVC
        vc.pinMode = .confirmPin(withInitialPin: appPin)
        vc.pinAutoConfirm = false
        vc.progress = nil
        vc.delegate = self
        vc.viewTitle = "SEND"
        vc.subTitle = "Enter PIN"
        vc.instructionText = ""
        vc.backButtonTitle = "<"
        vc.nextButtonTitle = "Confirm"
        self.navigationController.pushViewController(vc, animated: true)
    }
}


extension WalletCoordinator: PinVCDelegate {
    
    public func pinVCButtonNextTouched(pinEntered pin: String, viewController: PinVC) {
        guard let key = self.moneroBag.payment?.keyOfPendingTransaction else {
            let _todo = "notify that something went wrong"
            return
        }

        let _todo = "perform payment async"
        monero_commitPendingTransaction(key)

        self.goBackToWalletViewController()
    }
    
    public func pinVCButtonBackTouched() {
        self.navigationController.popViewController(animated: true)
    }
    
    private func goBackToWalletViewController() {
        self.navigationController.popToViewController(self.walletVC, animated: true)
        
        self.walletUpdated()
    }
}


extension WalletCoordinator: SettingsCoordinatorDelegate {
    
    public func settingsCoordinatorSettingsCompleted(settingsCoordinator: SettingsCoordinator) {
        if let walletVC = self.walletVC {
            self.navigationController.popToViewController(walletVC, animated: true)
        }
        self.remove(childCoordinator: settingsCoordinator)
        self.startFiatUpdates()
    }

    public func settingsCoordinatorWalletNuked(settingsCoordinator: SettingsCoordinator) {
        self.stopFeeEstimationUpdates()
        self.remove(childCoordinator: settingsCoordinator)
        self.delegate?.walletCoordinatorWalletNuked(walletCoordinator: self)
    }
}















