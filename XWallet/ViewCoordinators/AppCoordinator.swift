//
//  AppCoordinator.swift
//  XWallet
//
//  Created by loj on 22.10.17.
//

import Foundation
import UIKit


public class AppCoordinator: Coordinator {
    
    private var moneroBag: MoneroBagProtocol
    private var onboardingService: OnboardingServiceProtocol
    private var propertyStore: PropertyStoreProtocol
    private var secureStore: SecureStoreProtocol
    private var fileHandling: FileHandlingProtocol
    private var walletLifecycleService: WalletLifecycleServiceProtocol
    private var fiatService: FiatServiceProtocol
    private var feeService: FeeServiceProtocol
    
    private let navigationController: UINavigationController
    public var childCoordinators: [Coordinator] = []
    
    public init(navigationController: UINavigationController,
                moneroBag: MoneroBagProtocol,
                onboardingService: OnboardingServiceProtocol,
                propertyStore: PropertyStoreProtocol,
                secureStore: SecureStoreProtocol,
                fileHandling: FileHandlingProtocol,
                walletLifecycleService: WalletLifecycleServiceProtocol,
                fiatService: FiatServiceProtocol,
                feeService: FeeServiceProtocol)
    {
        self.navigationController = navigationController
        self.moneroBag = moneroBag
        self.onboardingService = onboardingService
        self.propertyStore = propertyStore
        self.secureStore = secureStore
        self.fileHandling = fileHandling
        self.walletLifecycleService = walletLifecycleService
        self.fiatService = fiatService
        self.feeService = feeService
    }
    
    public func start() {
        if self.hasWallet() {
            self.showLogin()
        } else {
            self.showOnboarding()
        }
    }
    
    private func hasWallet() -> Bool {
        return self.propertyStore.onboardingIsFinished
    }
    
    private func showLogin() {
        let loginCoordinator = LoginCoordinator(navigationController: self.navigationController,
                                                moneroBag: self.moneroBag,
                                                secureStore: self.secureStore,
                                                fileHandling: self.fileHandling,
                                                walletLifecycleService: self.walletLifecycleService)
        loginCoordinator.delegate = self
        loginCoordinator.start()
        
        self.add(childCoordinator: loginCoordinator)
    }
    
    private func showOnboarding() {
        let onboardingCoordinator = OnboardingCoordinator(navigationController: self.navigationController,
                                                          moneroBag: self.moneroBag,
                                                          onboardingService: self.onboardingService)
        onboardingCoordinator.delegate = self
        onboardingCoordinator.start()
        
        self.add(childCoordinator: onboardingCoordinator)
    }
    
    private func showWallet() {
        let walletCoordinator = WalletCoordinator(navigationController: self.navigationController,
                                                  moneroBag: self.moneroBag,
                                                  walletLifecycleService: self.walletLifecycleService,
                                                  propertyStore: self.propertyStore,
                                                  secureStore: self.secureStore,
                                                  fiatService: self.fiatService,
                                                  feeService: self.feeService)
        walletCoordinator.delegate = self
        walletCoordinator.start()
        
        self.add(childCoordinator: walletCoordinator)
    }
}


extension AppCoordinator: OnboardingCoordinatorDelegate  {

    public func onboardingCoordinatorLoginSucessful(onboardingCoordinator: OnboardingCoordinator) {
        self.navigationController.popToRootViewController(animated: false)
        self.remove(childCoordinator: onboardingCoordinator)
        self.showWallet()
    }
}


extension AppCoordinator: LoginCoordinatorDelegate {
    
    public func loginCoordinatorLoginSucessful(loginCoordinator: LoginCoordinator) {
        self.navigationController.popToRootViewController(animated: false)
        self.remove(childCoordinator: loginCoordinator)
        self.showWallet()
    }
}


extension AppCoordinator: WalletCoordinatorDelegate {
    
    public func walletCoordinatorWalletNuked(walletCoordinator: WalletCoordinator) {
        self.navigationController.popToRootViewController(animated: false)
        self.remove(childCoordinator: walletCoordinator)
        self.showOnboarding()
    }
}













