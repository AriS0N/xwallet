//
//  SettingsCoordinator.swift
//  XWallet
//
//  Created by loj on 21.01.18.
//

import Foundation
import UIKit


public protocol SettingsCoordinatorDelegate: class {
    func settingsCoordinatorSettingsCompleted(settingsCoordinator: SettingsCoordinator)
    func settingsCoordinatorWalletNuked(settingsCoordinator: SettingsCoordinator)
}


public class SettingsCoordinator: Coordinator {
    
    private let storyboardName = "Settings"
    private let settingsSceneName = "Settings"
    
    private let singleSelectTableViewStoryboardName = "SingleSelectTableView"
    private let singleSelectTableViewSceneName = "SingleSelectTableView"

    private let selectNodeStoryboardName = "SelectNode"
    private let selectNodeSceneName = "SelectNode"

    private let navigationController: UINavigationController
    public var childCoordinators: [Coordinator] = []
    
    public weak var delegate: SettingsCoordinatorDelegate?
    
    private var propertyStore: PropertyStoreProtocol
    private var secureStore: SecureStoreProtocol
    private var walletLifecycleService: WalletLifecycleServiceProtocol
    private var moneroBag: MoneroBagProtocol
    private var settingsVC: SettingsVC?
    
    init(navigationController: UINavigationController,
         propertyStore: PropertyStoreProtocol,
         secureStore: SecureStoreProtocol,
         moneroBag: MoneroBagProtocol,
         walletLifecycleService: WalletLifecycleServiceProtocol)
    {
        self.navigationController = navigationController
        self.propertyStore = propertyStore
        self.secureStore = secureStore
        self.moneroBag = moneroBag
        self.walletLifecycleService = walletLifecycleService
    }
    
    private lazy var storyboard: UIStoryboard = {
        let storyboard = UIStoryboard(name: self.storyboardName, bundle: nil)
        return storyboard
    }()

    private lazy var singleSelectTableViewStoryboard: UIStoryboard = {
        let storyboard = UIStoryboard(name: self.singleSelectTableViewStoryboardName, bundle: nil)
        return storyboard
    }()
    
    private lazy var selectNodeStoryboard: UIStoryboard = {
        let storyboard = UIStoryboard(name: selectNodeStoryboardName, bundle: nil)
        return storyboard
    }()
    
    public func start() {
        self.showSettingsViewController()
    }
    
    private func showSettingsViewController() {
        self.settingsVC = self.storyboard.instantiateViewController(withIdentifier: settingsSceneName) as? SettingsVC
        if let vc = self.settingsVC {
            vc.delegate = self
            vc.viewTitle = "SETTINGS"
            vc.backButtonTitle = ""
            
            vc.fiatConversionUnitsCellTitle = "Fiat Conversion Units"
            vc.fiatConversionUnitsSelectedValue = self.propertyStore.currency
            vc.languageCellTitle = "Language"
            vc.languageSelectedValue = self.propertyStore.language
            vc.displayRecoverySeedCellTitle = "Display Recovery Seed"
            vc.displayRecoverySeedCellSubTitle = "Ensure nobody can see your screen"
            vc.displayRecoverySeedCellButtonTitle = "Reveal"
            vc.resetPinCellTitle = "Reset PIN"
            vc.resetPinCellButtonTitle = "Change"
            vc.selectNodeCellTitle = "Select Node"
            vc.selectNodeCellSubTitle = "Select a node you trust"
            vc.selectNodeCellButtonTitle = "Choose"
            vc.nukeXWalletCellTitle = "Nuke X Wallet"
            vc.nukeXWalletCellSubTitle = "Remove all wallet data"
            vc.nukeXWalletCellButtonTitle = "Nuke"
            
            self.navigationController.pushViewController(vc, animated: true)
        }
    }
    
    private func showFiatConversionUnitsViewController() {
        let vc = self.singleSelectTableViewStoryboard.instantiateViewController(withIdentifier: singleSelectTableViewSceneName) as! SingleSelectionTableViewVC
        vc.delegate = self
        vc.viewTitle = "CURRENCY"
        vc.backButtonTitle = ""
        vc.cellValues = ["EUR", "USD", "CHF", "GBP"]
        vc.currentValue = self.propertyStore.currency
        vc.selectionChangedAction = { self.switchTo(currency: vc.currentValue) }
        
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    private func switchTo(currency: String?) {
        if let currency = currency {
            self.propertyStore.currency = currency
        }
        self.propertyStore.lastFiatFactor = nil
        self.propertyStore.lastFiatUpdate = nil
    }
    
    private func showLanguageViewController() {
        let vc = self.singleSelectTableViewStoryboard.instantiateViewController(withIdentifier: singleSelectTableViewSceneName) as! SingleSelectionTableViewVC
        vc.delegate = self
        vc.viewTitle = "LANGUAGE"
        vc.backButtonTitle = ""
        vc.cellValues = ["English", "German", "French", "Italian"]
        vc.currentValue = self.propertyStore.language
        vc.selectionChangedAction = { self.propertyStore.language = vc.currentValue! }
        
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    private func showSelectNodeViewController() {
        let vc = self.selectNodeStoryboard.instantiateViewController(withIdentifier: selectNodeSceneName) as! SelectNodeVC
        vc.delegate = self
        vc.viewTitle = "SELECT NODE"
        vc.backButtonTitle = ""
        vc.defaultsButtonTitle = "Default"
        vc.address = self.propertyStore.nodeAddress
        vc.addressLabelTitle = "ADDRESS"
        vc.userId = self.secureStore.nodeUserId
        vc.userIdLabelTitle = "USER ID"
        vc.password = self.secureStore.nodePassword
        vc.passwordLabelTitle = "PASSWORD"
        vc.connectButtonTitle = "Connect"
        
        self.navigationController.pushViewController(vc, animated: true)
    }
}


extension SettingsCoordinator: SettingsVCProtocol {
    
    func settingsVCBackButtonTouched() {
        self.delegate?.settingsCoordinatorSettingsCompleted(settingsCoordinator: self)
    }
    
    func settingsVCFiatConversionUnitsSelectionTouched() {
        self.showFiatConversionUnitsViewController()
    }
    
    func settingsVCLanguageSelectionTouched() {
        self.showLanguageViewController()
    }
    
    func settingsVCRevealRecoverySeedButtonTouched() {
        guard let wallet = self.moneroBag.wallet else { return }
        let revealSeedCoordinator = RevealSeedCoordinator(navigationController: self.navigationController,
                                                          secureStore: self.secureStore,
                                                          wallet: wallet)
        revealSeedCoordinator.delegate = self
        revealSeedCoordinator.start()
        
        self.add(childCoordinator: revealSeedCoordinator)
    }
    
    func settingsVCChangePinButtonTouched() {
        let changePinCoordinator = ChangePinCoordinator(navigationController: self.navigationController,
                                                        secureStore: self.secureStore)
        changePinCoordinator.delegate = self
        changePinCoordinator.start()
        
        self.add(childCoordinator: changePinCoordinator)
    }
    
    func settingsVCSelectNodeButtonTouched() {
        self.showSelectNodeViewController()
    }
    
    func settingsVCNukeXWalletButtonTouched() {
        guard let wallet = self.moneroBag.wallet else { return }
        let nukeWalletCoordinator = NukeWalletCoordinator(navigationController: self.navigationController,
                                                          wallet: wallet,
                                                          secureStore: self.secureStore,
                                                          propertyStore: self.propertyStore)
        nukeWalletCoordinator.delegate = self
        nukeWalletCoordinator.start()
        
        self.add(childCoordinator: nukeWalletCoordinator)
    }
}


extension SettingsCoordinator: SingleSelectionTableViewVCProtocol {
    
    func singleSelectionTableViewVCBackButtonTouched() {
        if let settingsVC = self.settingsVC {
            self.navigationController.popToViewController(settingsVC, animated: true)
        }

        self.settingsVC?.fiatConversionUnitsSelectedValue = self.propertyStore.currency
        self.settingsVC?.languageSelectedValue = self.propertyStore.language
        self.settingsVC?.refresh()
    }
}


extension SettingsCoordinator: RevealSeedCoordinatorDelegate {
    
    public func revealSeedCoordinatorDone(revealSeedCoordinator: RevealSeedCoordinator) {
        if let settingsVC = self.settingsVC {
            self.navigationController.popToViewController(settingsVC, animated: true)
        }
        self.remove(childCoordinator: revealSeedCoordinator)
    }
}


extension SettingsCoordinator: ChangePinCoordinatorDelegate {

    public func changePinCoordinatorDone(changePinCoordinator: ChangePinCoordinator) {
        if let settingsVC = self.settingsVC {
            self.navigationController.popToViewController(settingsVC, animated: true)
        }
        self.remove(childCoordinator: changePinCoordinator)
    }
}


extension SettingsCoordinator: NukeWalletCoordinatorDelegate {
    
    public func nukeWalletCoordinatorCancelled(nukeWalletCoordinator: NukeWalletCoordinator) {
        if let settingsVC = self.settingsVC {
            self.navigationController.popToViewController(settingsVC, animated: true)
        }
        self.remove(childCoordinator: nukeWalletCoordinator)
    }
    
    public func nukeWalletCoordinatorWalletNuked(nukeWalletCoordinator: NukeWalletCoordinator) {
        self.remove(childCoordinator: nukeWalletCoordinator)
        self.delegate?.settingsCoordinatorWalletNuked(settingsCoordinator: self)
    }
}

extension SettingsCoordinator: SelectNodeVCDelegate {
    
    func selectNodeVCBackButtonTouched() {
        if let settingsVC = self.settingsVC {
            self.navigationController.popToViewController(settingsVC, animated: true)
        }
    }
    
    func selectNodeVCRestoreDefaultsButtonTouched(selectNodeVC: SelectNodeVC) {
        selectNodeVC.address = Constants.defaultNodeAddress
        selectNodeVC.userId = Constants.defaultNodeUserId
        selectNodeVC.password = Constants.defaultNodePassword
        selectNodeVC.refresh()
    }
    
    func selectNodeVCConnectButtonTouched(address: String,
                                          userId: String,
                                          password: String,
                                          selectNodeVC: SelectNodeVC)
    {
        self.storeNodeSettings(address: address, userId: userId, password: password)
        self.redirectNode(selectNodeVC: selectNodeVC)
    }
    
    private func storeNodeSettings(address: String, userId: String, password: String) {
        self.propertyStore.nodeAddress = address
        self.secureStore.nodeUserId = userId
        self.secureStore.nodePassword = password
    }
    
    private func redirectNode(selectNodeVC: SelectNodeVC) {
        selectNodeVC.showActivityIndicator()
        
        DispatchQueue.global(qos: .background).async {
            self.closeWallet()
            self.reopenWalletWithNewDaemonConfig()
            
            DispatchQueue.main.async {
                selectNodeVC.hideActivityIndicator()
            }
        }
    }
    
    private func closeWallet() {
        guard let wallet = self.moneroBag.wallet else {
            return
        }
        self.walletLifecycleService.lock(wallet: wallet)
    }
    
    private func reopenWalletWithNewDaemonConfig() {
        guard let walletPassword = self.secureStore.walletPassword else {
            return
        }
        guard let wallet = self.walletLifecycleService.unlockWallet(withPassword: walletPassword) else {
            return
        }
        self.moneroBag.wallet = wallet
    }
}













