//
//  LoginCoordinator.swift
//  XWallet
//
//  Created by loj on 15.11.17.
//

import Foundation
import UIKit


public protocol LoginCoordinatorDelegate: class {
    func loginCoordinatorLoginSucessful(loginCoordinator: LoginCoordinator)
}


public class LoginCoordinator: Coordinator {
    
    private let storyboardName = "Onboarding"
    private let pinSceneName = "PIN"

    private var moneroBag: MoneroBagProtocol
    private var secureStore: SecureStoreProtocol
    private var fileHandling: FileHandlingProtocol
    private var walletLifecycleService: WalletLifecycleServiceProtocol
    
    private let navigationController: UINavigationController
    public var childCoordinators: [Coordinator] = []

    weak var delegate: LoginCoordinatorDelegate?
    
    private lazy var storyboard: UIStoryboard = {
        let storyboard = UIStoryboard(name: self.storyboardName, bundle: nil)
        return storyboard
    }()
    
    func start() {
        self.showPinViewController()
    }
    
    init(navigationController: UINavigationController,
         moneroBag: MoneroBagProtocol,
         secureStore: SecureStoreProtocol,
         fileHandling: FileHandlingProtocol,
         walletLifecycleService: WalletLifecycleServiceProtocol)
    {
        self.navigationController = navigationController
        self.moneroBag = moneroBag
        self.secureStore = secureStore
        self.fileHandling = fileHandling
        self.walletLifecycleService = walletLifecycleService
    }
    
    private func showPinViewController() {
        guard let appPin = self.secureStore.appPin else {
            return
        }
        
        let vc = self.storyboard.instantiateViewController(withIdentifier: pinSceneName) as! PinVC
        vc.pinMode = .confirmPin(withInitialPin: appPin)
        vc.pinAutoConfirm = true
        vc.progress = nil
        vc.delegate = self
        vc.viewTitle = "X WALLET"
        vc.subTitle = "Enter PIN"
        vc.instructionText = ""
        vc.backButtonTitle = nil
        vc.nextButtonTitle = ""
        self.navigationController.pushViewController(vc, animated: true)
    }
}


extension LoginCoordinator: PinVCDelegate {

    public func pinVCButtonNextTouched(pinEntered pin: String, viewController: PinVC) {
        viewController.showActivityIndicator()
        
        DispatchQueue.global(qos: .background).async {
            guard let walletPassword = self.secureStore.walletPassword,
                let wallet = self.walletLifecycleService.unlockWallet(withPassword: walletPassword) else
            {
                    let _todo = "show message that unlock failed"
                    return
            }
            self.moneroBag.wallet = wallet
            
            DispatchQueue.main.async {
                viewController.hideActivityIndicator()
                self.delegate?.loginCoordinatorLoginSucessful(loginCoordinator: self)
            }
        }
    }
    
    public func pinVCButtonBackTouched() {
        // perform no action as should not be called due to no back button visible
    }
    
}














