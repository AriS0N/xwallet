//
//  NewWalletCoordinator.swift
//  XWallet
//
//  Created by loj on 24.10.17.
//

import Foundation
import UIKit


public protocol OnboardingCoordinatorDelegate: class {
    func onboardingCoordinatorLoginSucessful(onboardingCoordinator: OnboardingCoordinator)
}


public class OnboardingCoordinator: Coordinator {
    
    private let storyboardName = "Onboarding"
    private let newWalletSceneName = "NewWallet"
    private let seedSceneName = "Seed"
    private let pinSceneName = "PIN"
    private let recoverSceneName = "Recover"

    private var moneroBag: MoneroBagProtocol
    private var onboardingService: OnboardingServiceProtocol

    private let navigationController: UINavigationController
    public var childCoordinators: [Coordinator] = []

    weak var delegate: OnboardingCoordinatorDelegate?

    private enum NewWalletMode {
        case fromScratch
        case recoverFromSeed
    }
    private var newWalletMode: NewWalletMode = .fromScratch
    
    private var isInitialPin = true
    
    private lazy var storyboard: UIStoryboard = {
        let storyboard = UIStoryboard(name: self.storyboardName, bundle: nil)
        return storyboard
    }()

    func start() {
        self.showNewWalletView()
    }

    init(navigationController: UINavigationController,
         moneroBag: MoneroBagProtocol,
         onboardingService: OnboardingServiceProtocol)
    {
        self.navigationController = navigationController
        self.moneroBag = moneroBag
        self.onboardingService = onboardingService
    }
    
    private func showNewWalletView() {
        let vc = self.storyboard.instantiateViewController(withIdentifier: self.newWalletSceneName) as! NewWalletVC
        vc.delegate = self
        vc.newWalletButtonTitle = "New Wallet"
        vc.recoverWalletButtonTitle = "Recover Wallet"
        self.navigationController.pushViewController(vc, animated: true)
    }
}


extension OnboardingCoordinator: NewWalletVCDelegate {
    
    public func newWalletVCDidSelectNewWallet(newWallet: NewWalletVC) {
        self.newWalletMode = .fromScratch
        self.showPinViewController()
    }
    
    public func newWalletVCDidSelectRecoverWallet(newWallet: NewWalletVC) {
        self.newWalletMode = .recoverFromSeed
        self.showPinViewController()
    }
    
    private func showPinViewController() {
        let vc = self.storyboard.instantiateViewController(withIdentifier: pinSceneName) as! PinVC
        vc.pinMode = .initialPin
        vc.progress = 0.333
        vc.delegate = self
        vc.viewTitle = "X WALLET"
        vc.subTitle = "PIN"
        vc.instructionText = "Set a PIN. If you forget your PIN you must restore your wallet from the seed you wrote down."
        vc.backButtonTitle = "<"
        vc.nextButtonTitle = "Next"
        self.navigationController.pushViewController(vc, animated: true)
    }
    
}


extension OnboardingCoordinator: PinVCDelegate {
    
    public func pinVCButtonNextTouched(pinEntered appPin: String, viewController: PinVC) {
        if self.isInitialPin {
            self.showPinConfirmViewController(initialPin: appPin)
        } else {
            self.onboardingService.setAppPin(appPin)
            self.onboardingService.setWalletName(Constants.defaultWalletName)
            
            switch self.newWalletMode {
            case .fromScratch:
                self.showSeedViewController()
            case .recoverFromSeed:
                self.showRecoverySeedViewController()
            }
        }
    }
    
    public func pinVCButtonBackTouched() {
        self.isInitialPin = true
        self.navigationController.popViewController(animated: true)
    }
    
    private func showPinConfirmViewController(initialPin: String) {
        self.isInitialPin = false
        
        let vc = self.storyboard.instantiateViewController(withIdentifier: pinSceneName) as! PinVC
        vc.pinMode = .confirmPin(withInitialPin: initialPin)
        vc.progress = 0.666
        vc.delegate = self
        vc.viewTitle = "X WALLET"
        vc.subTitle = "Confirm PIN"
        vc.instructionText = "Confirm your PIN which you entered before."
        vc.backButtonTitle = "<"
        vc.nextButtonTitle = "Next"
        self.navigationController.pushViewController(vc, animated: false)
    }
    
    private func showSeedViewController() {
        guard let wallet = try? self.onboardingService.createNewWallet() else {
            // TODO: (loj) show popup with error message
            return
        }
        self.moneroBag.wallet = wallet
        
        let vc = self.storyboard.instantiateViewController(withIdentifier: self.seedSceneName) as! SeedVC
        vc.delegate = self
        vc.seed = wallet.seed
        vc.progress = 1.0
        vc.backButtonTitle = "<"
        vc.nextButtonTitle = "Create"
        vc.viewTitle = "X WALLET"
        vc.subTitle = "Seed"
        vc.instructionText = "Write down the following 25 words.\nThis is the only way to restore your wallet if you lose your phone or forget your PIN."
        self.navigationController.pushViewController(vc, animated: false)
    }
    
    private func showRecoverySeedViewController() {
        let vc = self.storyboard.instantiateViewController(withIdentifier: self.recoverSceneName) as! RecoverSeedVC
        vc.delegate = self
        vc.progress = 1.0
        vc.backButtonTitle = "<"
        vc.nextButtonTitle = "Create"
        vc.subTitle = "Import Seed"
        vc.instructionText = "Enter your 25 words seed. One word at a time. Confirm with return."
        self.navigationController.pushViewController(vc, animated: false)
    }
    
}


extension OnboardingCoordinator: SeedVCDelegeta {
    
    func seedVCButtonNextTouched() {
        self.delegate?.onboardingCoordinatorLoginSucessful(onboardingCoordinator: self)
    }
    
    func seedVCButtonBackTouched() {
        self.onboardingService.purgeWallet()
        self.navigationController.popViewController(animated: true)
    }
    
}


extension OnboardingCoordinator: RecoverSeedVCDelegeta {

    func recoverSeedVCButtonNextTouched(seed: Seed) {
        self.onboardingService.setSeed(seed)
        
        guard let wallet = try? self.onboardingService.recoverWallet() else {
            // TODO: (loj) show popup with error message
            return
        }
        self.moneroBag.wallet = wallet
        self.delegate?.onboardingCoordinatorLoginSucessful(onboardingCoordinator: self)
    }
    
    func recoverSeedVCButtonBackTouched() {
        self.navigationController.popViewController(animated: true)
    }

}





















