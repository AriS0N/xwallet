//
//  SettingsVC.swift
//  XWallet
//
//  Created by loj on 21.01.18.
//

import UIKit


protocol SettingsVCProtocol: class {
    func settingsVCBackButtonTouched()
    func settingsVCFiatConversionUnitsSelectionTouched()
    func settingsVCLanguageSelectionTouched()
    func settingsVCRevealRecoverySeedButtonTouched()
    func settingsVCChangePinButtonTouched()
    func settingsVCSelectNodeButtonTouched()
    func settingsVCNukeXWalletButtonTouched()
}


class SettingsVC: UIViewController {
    
    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func backButtonTouched() {
        self.delegate?.settingsVCBackButtonTouched()
    }
    
    public weak var delegate: SettingsVCProtocol?
    
    public var viewTitle: String?
    public var backButtonTitle: String?
    
    public var fiatConversionUnitsCellTitle: String?
    public var fiatConversionUnitsSelectedValue: String?
    public var languageCellTitle: String?
    public var languageSelectedValue: String?
    public var displayRecoverySeedCellTitle: String?
    public var displayRecoverySeedCellSubTitle: String?
    public var displayRecoverySeedCellButtonTitle: String?
    public var resetPinCellTitle: String?
    public var resetPinCellButtonTitle: String?
    public var selectNodeCellTitle: String?
    public var selectNodeCellSubTitle: String?
    public var selectNodeCellButtonTitle: String?
    public var nukeXWalletCellTitle: String?
    public var nukeXWalletCellSubTitle: String?
    public var nukeXWalletCellButtonTitle: String?

    public func refresh() {
        self.updateView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateControls()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    private func setup() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    private func updateControls() {
        if let viewTitle = self.viewTitle {
            self.viewTitleLabel.text = viewTitle
        }
        if let backButtonTitle = self.backButtonTitle {
            self.backButton.setTitle(backButtonTitle, for: .normal)
        }
    }
    
    private func updateView() {
        self.tableView.reloadData()
    }
    
    private enum CellIdentifier: String {
        case selectionCell = "SelectionCell"
        case actionCell = "ActionCell"
        case actionCellWithSubTitle = "ActionCellWithSubTitle"
        case warningCellWithSubTitle = "WarningCellWithSubTitle"
    }
    
//    private var cellDefinitions: [Int:(UITableViewCell, Int, (() -> Void)?)] {
//        get {
//            return [0: (fiatConversionUnitsCell, 104, self.delegate?.settingsVCFiatConversionUnitsSelectionTouched),
//                    1: (languageCell, 104, self.delegate?.settingsVCLanguageSelectionTouched),
//                    2: (displayRecoverySeedCell, 142, nil),
//                    3: (changePinCell, 122, nil),
//                    4: (selectNodeCell, 142, nil),
//                    5: (nukeXWalletCell, 142, nil)]
//        }
//    }
    private var cellDefinitions: [Int:(UITableViewCell, Int, (() -> Void)?)] {
        get {
            return [0: (fiatConversionUnitsCell, 104, self.delegate?.settingsVCFiatConversionUnitsSelectionTouched),
                    1: (displayRecoverySeedCell, 142, nil),
                    2: (changePinCell, 122, nil),
                    3: (selectNodeCell, 142, nil),
                    4: (nukeXWalletCell, 142, nil)]
        }
    }

    private var fiatConversionUnitsCell: UITableViewCell {
        get {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: CellIdentifier.selectionCell.rawValue)
                as! SelectionCell
            if let cellTitle = self.fiatConversionUnitsCellTitle {
                cell.cellTitle = cellTitle
            }
            if let selectedValue = self.fiatConversionUnitsSelectedValue {
                cell.selectedValue = selectedValue
            }
            cell.buttonTouchedHandler = self.delegate?.settingsVCFiatConversionUnitsSelectionTouched
            cell.redraw()
            return cell
        }
    }

    private var languageCell: UITableViewCell {
        get {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: CellIdentifier.selectionCell.rawValue)
                as! SelectionCell
            if let cellTitle = languageCellTitle {
                cell.cellTitle = cellTitle
            }
            if let selectedValue = self.languageSelectedValue {
                cell.selectedValue = selectedValue
            }
            cell.buttonTouchedHandler = self.delegate?.settingsVCLanguageSelectionTouched
            cell.redraw()
            return cell
        }
    }
    
    private lazy var displayRecoverySeedCell: UITableViewCell = {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: CellIdentifier.actionCellWithSubTitle.rawValue)
            as! ActionCellWithSubTitle
        if let cellTitle = self.displayRecoverySeedCellTitle {
            cell.cellTitle = cellTitle
        }
        if let subTitle = self.displayRecoverySeedCellSubTitle {
            cell.cellSubTitle = subTitle
        }
        if let buttonTitle = self.displayRecoverySeedCellButtonTitle {
            cell.buttonTitle = buttonTitle
        }
        cell.buttonTouchedHandler = self.delegate?.settingsVCRevealRecoverySeedButtonTouched
        cell.redraw()
        return cell
    }()
    
    private lazy var changePinCell: UITableViewCell = {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: CellIdentifier.actionCell.rawValue)
            as! ActionCell
        if let cellTitle = self.resetPinCellTitle {
            cell.cellTitle = cellTitle
        }
        if let buttonTitle = self.resetPinCellButtonTitle {
            cell.buttonTitle = buttonTitle
        }
        cell.buttonTouchedHandler = self.delegate?.settingsVCChangePinButtonTouched
        cell.redraw()
        return cell
    }()

    private lazy var selectNodeCell: UITableViewCell = {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: CellIdentifier.actionCellWithSubTitle.rawValue)
            as! ActionCellWithSubTitle
        if let cellTitle = self.selectNodeCellTitle {
            cell.cellTitle = cellTitle
        }
        if let subTitle = self .selectNodeCellSubTitle {
            cell.cellSubTitle = subTitle
        }
        if let buttonTitle = self .selectNodeCellButtonTitle {
            cell.buttonTitle = buttonTitle
        }
        cell.buttonTouchedHandler = self.delegate?.settingsVCSelectNodeButtonTouched
        cell.redraw()
        return cell
    }()
    
    private lazy var nukeXWalletCell: UITableViewCell = {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: CellIdentifier.warningCellWithSubTitle.rawValue)
            as! WarningCellWithSubTitle
        if let cellTitle = self .nukeXWalletCellTitle {
            cell.cellTitle = cellTitle
        }
        if let subTitle = self.nukeXWalletCellSubTitle {
            cell.cellSubTitle = subTitle
        }
        if let buttonTitle = self.nukeXWalletCellButtonTitle {
            cell.buttonTitle = buttonTitle
        }
        cell.buttonTouchedHandler = self.delegate?.settingsVCNukeXWalletButtonTouched
        cell.redraw()
        return cell
    }()
    
}


extension SettingsVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let actionOnSelection = self.cellDefinitions[indexPath.row]?.2 {
            actionOnSelection()
        }
    }
}


extension SettingsVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellDefinitions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellDefinition = self.cellDefinitions[indexPath.row]
        let cell = cellDefinition?.0
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellDefinition = self.cellDefinitions[indexPath.row]
        let height = cellDefinition?.1
        return CGFloat(height!)
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let cellDefinition = self.cellDefinitions[indexPath.row]
        if let _ = cellDefinition?.2 {
            return true
        }
        return false
    }
}










