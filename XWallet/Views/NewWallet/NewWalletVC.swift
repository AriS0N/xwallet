//
//  ViewController.swift
//  XWallet
//
//  Created by loj on 26.07.17.
//

import UIKit


public protocol NewWalletVCDelegate: class {
    func newWalletVCDidSelectNewWallet(newWallet: NewWalletVC)
    func newWalletVCDidSelectRecoverWallet(newWallet: NewWalletVC)
}


public class NewWalletVC: UIViewController {
    
    @IBOutlet weak var newWalletButton: UIButton!
    @IBOutlet weak var recoverWalletButton: UIButton!
    
    public weak var delegate: NewWalletVCDelegate?
    
    public var newWalletButtonTitle: String?
    public var recoverWalletButtonTitle: String?
    
    @IBAction func newWalletButtonTouched() {
        self.delegate?.newWalletVCDidSelectNewWallet(newWallet: self)
    }
    
    @IBAction func recoverWalletButtonTouched() {
        self.delegate?.newWalletVCDidSelectRecoverWallet(newWallet: self)
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateView()
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func updateView() {
        if let newWalletButtonTitle = self.newWalletButtonTitle {
            self.newWalletButton.setTitle(newWalletButtonTitle, for: .normal)
        }
        if let recoverWalletButtonTitle = self.recoverWalletButtonTitle {
            self.recoverWalletButton.setTitle(recoverWalletButtonTitle, for: .normal)
        }
    }
}

