//
//  WalletVC.swift
//  XWallet
//
//  Created by loj on 16.11.17.
//

import UIKit


protocol WalletVCDelegate: class {
    func walletVCSettingsButtonTouched()
    func walletVCSendTouched()
    func walletVCReceiveTouched()
    func walletVCWillEnterForeground()
}


class WalletVC: UIViewController {
    
    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var xmrAmountLabel: UILabel!
    @IBOutlet weak var otherAmountLabel: UILabel!
    @IBOutlet weak var configButton: UIButton!
    @IBOutlet weak var sendButtonLabel: UILabel!
    @IBOutlet weak var sendButtonImageView: UIImageView!
    @IBOutlet weak var receiveButtonLabel: UILabel!
    @IBOutlet weak var receiveButtonImageView: UIImageView!
    @IBOutlet weak var historyTableView: UITableView!

    // Container handler used for gesture recognizer
    @IBOutlet weak var sendButtonStackView: UIStackView!
    @IBOutlet weak var receiveButtonStackView: UIStackView!
    
    @IBAction func settingsButtonTouched() {
        self.delegate?.walletVCSettingsButtonTouched()
    }
    
    @objc func sendButtonTouched() {
        self.delegate?.walletVCSendTouched()
    }
    
    @objc func receiveButtonTouched() {
        self.delegate?.walletVCReceiveTouched()
    }
    
    public weak var delegate: WalletVCDelegate?
    
    public var viewTitle: String?
    public var viewTitleSyncing: String?
    public var configButtonTitle: String?
    public var sendButtonTitle: String?
    public var receiveButtonTitle: String?
    
    public var viewModel: WalletViewModel?
    
    private var sendButtonGestureRecognizer = UITapGestureRecognizer()
    private var receiveButtonGestureRecognizer = UITapGestureRecognizer()
    
    private var syncIsInProgress = false
    private var hasLockedBalance = false
    
    public func viewModelIsUpdated() {
//        print("############# updating view due to updated viewmodel, triggered by listener")
        self.showData()
        
        self.hasLockedBalance = self.viewModel?.hasLockedBalance ?? false
        self.updateSendButtons()
    }

    public func walletSyncing(initialHeight: UInt64, walletHeight: UInt64, blockChainHeight: UInt64) {
        let min = Double(initialHeight)
        let max = Double(blockChainHeight)
        let current = Double(walletHeight)
        
        var percent = Int((current - min) / (max - min) * 100.0)
        if percent > 99 {
            percent = 99
        }
        if percent < 0 {
            percent = 0
        }
        
        if let viewTitleSyncing = self.viewTitleSyncing {
            self.viewTitleLabel.text = "\(viewTitleSyncing) \(percent)%"
        } else {
            self.viewTitleLabel.text = "\(percent)%"
        }
        
        self.progressView.setProgress(Float(percent) / 100.0, animated: true)
        self.progressView.isHidden = false
        
        self.syncIsInProgress = true
        self.updateSendButtons()
    }
    
    public func walletSyncCompleted() {
        if let viewTitle = self.viewTitle {
            self.viewTitleLabel.text = viewTitle
        }
        self.progressView.isHidden = true
        
        self.syncIsInProgress = false
        self.updateSendButtons()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateControls()
        self.showData()
        
        self.willEnterForeground()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc private func willEnterForeground() {
        self.delegate?.walletVCWillEnterForeground()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func setup() {
        self.historyTableView.delegate = self
        self.historyTableView.dataSource = self
        
        self.sendButtonGestureRecognizer.addTarget(self, action: #selector(self.sendButtonTouched))
        self.sendButtonStackView.addGestureRecognizer(self.sendButtonGestureRecognizer)
        
        self.receiveButtonGestureRecognizer.addTarget(self, action: #selector(self.receiveButtonTouched))
        self.receiveButtonStackView.addGestureRecognizer(self.receiveButtonGestureRecognizer)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.willEnterForeground),
                                               name: .UIApplicationWillEnterForeground,
                                               object: nil)
    }

    private func updateControls() {
        if let viewTitle = self.viewTitle {
            self.viewTitleLabel.text = viewTitle
        }
        self.progressView.isHidden = true
        if let configButtonTitle = self.configButtonTitle {
            self.configButton.setTitle(configButtonTitle, for: .normal)
        }
        if let sendButtonTitle = self.sendButtonTitle {
            self.sendButtonLabel.text = sendButtonTitle
        }
        if let receiveButtonTitle = self.receiveButtonTitle {
            self.receiveButtonLabel.text = receiveButtonTitle
        }
    }
    
    private func showData() {
        guard let viewModel = self.viewModel else {
            self.showEmptyData()
            return
        }
        
        // Listener might fire before view is completly initialized
        guard let xmrAmountLabel = self.xmrAmountLabel,
            let otherAmountLabel = self.otherAmountLabel,
            let historyTableView = self.historyTableView else { return }
        
        xmrAmountLabel.text = viewModel.xmrAmount
        otherAmountLabel.text = "\(viewModel.otherCurrency) \(viewModel.otherAmount)"
        historyTableView.reloadData()
    }
    
    private func showEmptyData() {
        self.xmrAmountLabel.text = "---"
        self.otherAmountLabel.text = "---"
        self.historyTableView.reloadData()
    }
    
    private func updateSendButtons() {
        if self.hasLockedBalance || self.syncIsInProgress {
            self.disableSendButton()
        } else {
            self.enableSendButton()
        }
    }
    
    private func enableSendButton() {
        self.sendButtonStackView.isUserInteractionEnabled = true
        self.sendButtonStackView.alpha = 1.0
    }
    
    private func disableSendButton() {
        self.sendButtonStackView.isUserInteractionEnabled = false
        self.sendButtonStackView.alpha = 0.5
    }
}


extension WalletVC: UITableViewDelegate {
}


extension WalletVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = self.viewModel else {
            return 0
        }
        
        return viewModel.history.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.historyTableView.dequeueReusableCell(withIdentifier: "TrxCell") as! TransactionCell

        guard let viewModel = self.viewModel else {
            return cell
        }
        
        let cellData = viewModel.history[indexPath.row]
        cell.direction = cellData.direction
        cell.isPending = cellData.isPending
        cell.isFailed = cellData.isFailed
        cell.trxAmount = cellData.readableAmountWithNetworkFee()
        cell.confirmations = cellData.confirmations
        cell.trxTimestamp = cellData.readableTimestamp()
        cell.redraw()
        return cell
    }
}














