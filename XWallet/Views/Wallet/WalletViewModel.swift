//
//  WalletViewModel.swift
//  XWallet
//
//  Created by loj on 19.11.17.
//

import Foundation


public struct WalletViewModel {
    
    public let xmrAmount: String
    public let otherAmount: String
    public let otherCurrency: String
    public let history: [TransactionItem]
    public let hasLockedBalance: Bool
    
    public init(xmrAmount: String,
                otherAmount: String,
                otherCurrency: String,
                history: [TransactionItem],
                hasLockedBalance: Bool)
    {
        self.xmrAmount = xmrAmount
        self.otherAmount = otherAmount
        self.otherCurrency = otherCurrency
        self.history = history
        self.hasLockedBalance = hasLockedBalance
    }
}
