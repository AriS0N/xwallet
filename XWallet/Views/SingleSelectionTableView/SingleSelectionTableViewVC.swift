//
//  SingleSelectionTableViewVC.swift
//  XWallet
//
//  Created by loj on 22.01.18.
//

import UIKit

protocol SingleSelectionTableViewVCProtocol: class {
    func singleSelectionTableViewVCBackButtonTouched()
}


class SingleSelectionTableViewVC: UIViewController {
    
    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func backButtonTouched() {
        self.delegate?.singleSelectionTableViewVCBackButtonTouched()
    }
    
    public weak var delegate: SingleSelectionTableViewVCProtocol?
    
    public var viewTitle: String?
    public var backButtonTitle: String?
    public var cellValues: [String]?
    public var currentValue: String?
    
    public var selectionChangedAction: (() -> Void)?
    
    private let cellIdentifier = "SingleSelectionCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateControls()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    private func setup() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    private func updateControls() {
        if let viewTitle = self.viewTitle {
            self.viewTitleLabel.text = viewTitle
        }
        if let backButtonTitle = self.backButtonTitle {
            self.backButton.setTitle(backButtonTitle, for: .normal)
        }
        
        self.tableView.reloadData()
    }
}


extension SingleSelectionTableViewVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.currentValue = self.cellValues?[indexPath.row]
        self.selectionChangedAction?()
        tableView.reloadData()
    }
}


extension SingleSelectionTableViewVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellValues?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SingleSelectionCell
        if let cellValue = self.cellValues?[indexPath.row] {
            cell.value = cellValue
            if cellValue == self.currentValue {
                cell.checkmarkIsSet = true
            } else {
                cell.checkmarkIsSet = false
            }
        }
        cell.redraw()
        return cell
    }
}










