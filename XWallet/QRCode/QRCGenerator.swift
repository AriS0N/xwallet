//
//  QRCGenerator.swift
//  XWallet
//
//  Created by loj on 26.11.17.
//

import CoreImage
import Foundation
import UIKit


public class QRCGenerator {
    
    public static func generate(from string: String?) -> UIImage? {
        if let filter = CIFilter(name: "CIQRCodeGenerator"),
            let data = string?.data(using: String.Encoding.ascii)
        {
            filter.setValue(data, forKey: "inputMessage")
            filter.setValue("H", forKey: "inputCorrectionLevel") // L(ow) M(edium) Q H(igh)
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
}
